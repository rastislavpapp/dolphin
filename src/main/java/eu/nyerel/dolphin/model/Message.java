package eu.nyerel.dolphin.model;

import java.io.Serializable;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class Message implements Serializable {

    private String objectName;
    private Class interfaceClass;
    private MethodCall call;

    public Class getInterfaceClass() {
        return interfaceClass;
    }

    public void setInterfaceClass(Class interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public MethodCall getCall() {
        return call;
    }

    public void setCall(MethodCall call) {
        this.call = call;
    }

}
