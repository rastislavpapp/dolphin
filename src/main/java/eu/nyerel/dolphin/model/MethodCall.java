package eu.nyerel.dolphin.model;

import java.io.Serializable;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class MethodCall implements Serializable {

    private String methodName;
    private Serializable[] parameters;

    public MethodCall() {
    }

    public MethodCall(String methodName, Serializable[] parameters) {
        this.methodName = methodName;
        this.parameters = parameters;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Serializable[] getParameters() {
        return parameters;
    }

    public void setParameters(Serializable[] parameters) {
        this.parameters = parameters;
    }

    public Object[] getArgs() {
        Object[] args = new Object[parameters.length];
        System.arraycopy(parameters, 0, args, 0, parameters.length);
        return args;
    }

    public String toString() {
        return methodName + "(" + formatParameters() + ")";
    }

    private String formatParameters() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parameters.length; i++) {
            sb.append(parameters[i].getClass().getName());
            if (i < parameters.length - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

}
