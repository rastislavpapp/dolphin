package eu.nyerel.dolphin.exception;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class ExportedObjectNotFound extends DolphinException {

    public ExportedObjectNotFound(String message) {
        super(message);
    }

}
