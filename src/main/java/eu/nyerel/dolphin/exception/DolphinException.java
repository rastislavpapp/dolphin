package eu.nyerel.dolphin.exception;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class DolphinException extends RuntimeException {

    public DolphinException() {
    }

    public DolphinException(String message) {
        super(message);
    }

    public DolphinException(String message, Throwable cause) {
        super(message, cause);
    }

    public DolphinException(Throwable cause) {
        super(cause);
    }

}
