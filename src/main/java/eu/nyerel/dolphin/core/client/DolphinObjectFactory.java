package eu.nyerel.dolphin.core.client;

import eu.nyerel.dolphin.core.server.DolphinServer;
import eu.nyerel.dolphin.exception.DolphinException;
import eu.nyerel.dolphin.model.Message;
import eu.nyerel.dolphin.model.MethodCall;
import eu.nyerel.dolphin.model.MethodCallResponse;
import eu.nyerel.dolphin.utils.IOUtils;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * Factory responsible for creating object stubs which communicate with the objects exported on server.
 *
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class DolphinObjectFactory {

    private final String host;
    private final int port;

    public DolphinObjectFactory() {
        this("localhost");
    }

    public DolphinObjectFactory(String host) {
        this(host, DolphinServer.DEFAULT_PORT);
    }

    public DolphinObjectFactory(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * Create instance of the given interface. Server looks up the instance amongst its exported objects and returns
     * first one that conforms to the interface. If you need more precise control over which instance is returned,
     * use {@link #create(String, Class)}, supplying objectName parameter.
     *
     * All methods of this interface are stubbed, and each invocation means communication with the server.
     *
     * Calling this method does not cause any communication with the server, and it is not guaranteed that the object
     * is valid. Exceptions will occur after calling methods of the object.
     *
     * @param objectInterface interface create stub for
     * @return object with the given interface
     */
    public <T> T create(Class<T> objectInterface) {
        return create(null, objectInterface);
    }

    /**
     * Create instance of the given interface. Server looks up the object by name, specified by {@code objectName}
     * parameter.
     *
     * All methods of this interface are stubbed, and each invocation means communication with the server.
     *
     * Calling this method does not cause any communication with the server, and it is not guaranteed that the object
     * is valid. Exceptions will occur after calling methods of the object.
     *
     * @param objectName name of the object exported on server
     * @param objectInterface interface create stub for
     * @return object with the given interface
     */
    public <T> T create(String objectName, Class<T> objectInterface) {
        Object instance = Proxy.newProxyInstance(objectInterface.getClassLoader(),
                new Class[]{ objectInterface },
                createInvocationHandler(objectName, objectInterface));
        return (T) instance;
    }

    private InvocationHandler createInvocationHandler(final String objectName, final Class objectInterface) {
        return new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Serializable[] serializableArgs = getSerializableArgs(args);
                Message message = new Message();
                message.setObjectName(objectName);
                message.setInterfaceClass(objectInterface);
                message.setCall(new MethodCall(method.getName(), serializableArgs));
                MethodCallResponse response = sendMessage(message);
                if (response.getException() != null) {
                    throw new DolphinException("Remote call resulted in exception", response.getException());
                } else {
                    return response.getValue();
                }
            }
        };
    }

    private MethodCallResponse sendMessage(Message message) {
        ObjectInputStream is = null;
        ObjectOutputStream os = null;
        Socket echoSocket = null;
        try {
            echoSocket = new Socket(host, port);
            os = new ObjectOutputStream(echoSocket.getOutputStream());
            is = new ObjectInputStream(echoSocket.getInputStream());
            os.writeObject(message);
            return (MethodCallResponse) is.readObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(echoSocket);
            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(is);
        }
    }

    private Serializable[] getSerializableArgs(Object[] args) {
        if (args != null) {
            Serializable[] serializableArgs = new Serializable[args.length];
            for (int i = 0; i < args.length; i++) {
                Object arg = args[i];
                if (arg instanceof Serializable) {
                    serializableArgs[i] = (Serializable) arg;
                } else {
                    throw new IllegalArgumentException("Argument of type " + arg.getClass().getName() + " is not serializable!");
                }
            }
            return serializableArgs;
        } else {
            return new Serializable[0];
        }
    }

}
