package eu.nyerel.dolphin.core.server;

import eu.nyerel.dolphin.model.Message;
import eu.nyerel.dolphin.utils.IOUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
class SocketListener {

    private MessageHandler messageHandler;
    private ServerSocket serverSocket;

    void listen(int port) {

        Socket clientSocket = null;
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                try {
                    try {
                        clientSocket = serverSocket.accept();
                    } catch (SocketException e) {
                        break;
                    }
                    out = new ObjectOutputStream(clientSocket.getOutputStream());
                    in = new ObjectInputStream(clientSocket.getInputStream());

                    Message message = (Message) in.readObject();
                    Serializable response = messageHandler.respond(message);
                    out.writeObject(response);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                } finally {
                    IOUtils.closeQuietly(clientSocket);
                    IOUtils.closeQuietly(out);
                    IOUtils.closeQuietly(in);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(serverSocket);
        }
    }

    void stop() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    void setMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

}
