package eu.nyerel.dolphin.core.server;

import eu.nyerel.dolphin.model.Message;

import java.io.Serializable;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
interface MessageHandler {

    Serializable respond(Message message);

}
