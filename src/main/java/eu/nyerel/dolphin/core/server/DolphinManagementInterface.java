package eu.nyerel.dolphin.core.server;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public interface DolphinManagementInterface {

    /**
     * Stop the server
     */
    void stop();

    /**
     * Check if the server is running
     * @return true if server is running, false otherwise
     */
    boolean isRunning();

}
