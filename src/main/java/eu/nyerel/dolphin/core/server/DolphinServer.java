package eu.nyerel.dolphin.core.server;

import eu.nyerel.dolphin.exception.ExportedObjectNotFound;
import eu.nyerel.dolphin.model.Message;
import eu.nyerel.dolphin.model.MethodCall;
import eu.nyerel.dolphin.model.MethodCallResponse;
import eu.nyerel.dolphin.utils.ReflectionUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Class representing a server. It listens on a port for requests from clients, and dispatches
 * those requests to objects exported via via {@link #export(String, Object)} method.
 *
 * Server starts listening after {@link #start()} method is called.
 *
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class DolphinServer {

    public static final int DEFAULT_PORT = 12322;
    public static final String MANAGEMENT_INTERFACE_DESTINATION = "managementInterface";

    private Map<String, Object> exportedObjects = new HashMap<String, Object>();

    private int port = DEFAULT_PORT;
    private boolean exportManagementInterface = true;

    private SocketListener socketListener;

    /**
     * Creates a server with default port, {@link #DEFAULT_PORT}
     *
     * Does not start the server, use {@link #start()} method.
     */
    public DolphinServer() {
    }

    /**
     * Creates a server with the given port.
     *
     * Does not start the server, use {@link #start()} method.
     */
    public DolphinServer(int port) {
        this.port = port;
    }

    /**
     * Set the port on which the server will listen.
     *
     * Has no effect after the {@link #start()} has been called.
     *
     * @param port port number. Default is {@link #DEFAULT_PORT}.
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Decides whether the server automatically exports its management interface, {@link DolphinManagementInterface}
     *
     * @param exportManagementInterface if true, the management interface will be exported. Default is true.
     */
    public void setExportManagementInterface(boolean exportManagementInterface) {
        this.exportManagementInterface = exportManagementInterface;
    }

    /**
     * Sets the objects which will be exported. Key is objectName, which can be specified on client, see
     * {@link eu.nyerel.dolphin.core.client.DolphinObjectFactory#create(String, Class)}
     *
     * Replaces all objects added by the {@link #export} methods before.
     *
     * Cannot be used if the server is running.
     *
     * @param exportedObjects map of exported objects
     */
    public void setExportedObjects(Map<String, Object> exportedObjects) {
        checkExportedObjectsModificationAtRuntime();
        this.exportedObjects = exportedObjects;
    }

    /**
     * Adds object to exported objects. Name will be generated.
     *
     * Cannot be used if the server is running.
     *
     * @param object object to export
     * @return generated object name
     */
    public String export(Object object) {
        checkExportedObjectsModificationAtRuntime();
        String uuid = UUID.randomUUID().toString();
        export(uuid, object);
        return uuid;
    }

    /**
     * Adds object to exported objects with the given name.
     *
     * @param name name of the object to export, to be used in {@link eu.nyerel.dolphin.core.client.DolphinObjectFactory#create(String, Class)}
     * @param object object to export
     */
    public void export(String name, Object object) {
        checkExportedObjectsModificationAtRuntime();
        exportedObjects.put(name, object);
    }

    /**
     * Starts the server.
     */
    public void start() {
        if (exportManagementInterface) {
            exportManagementInterface();
        }
        if (socketListener == null) {
            socketListener = new SocketListener();
            socketListener.setMessageHandler(new MessageHandler() {

                public Serializable respond(Message message) {
                    MethodCallResponse response = new MethodCallResponse();
                    try {
                        Object instance = findExportedObject(message);
                        response.setValue(invoke(instance, message.getCall()));
                    } catch (Exception e) {
                        response.setException(e);
                    }
                    return response;
                }

            });
            new Thread(new Runnable() {
                public void run() {
                    socketListener.listen(port);
                }
            }, "DolphinServer").start();
        }
    }

    private void checkExportedObjectsModificationAtRuntime() {
        if (isRunning()) {
            throw new IllegalStateException("Cannot change exported objects if the server is running!");
        }
    }

    private Object findExportedObject(Message message) {
        String name = message.getObjectName();
        if (name != null) {
            Object object = exportedObjects.get(name);
            if (object == null) {
                throw new ExportedObjectNotFound("No object with name " + name +
                        " found");
            }
            return object;
        } else {
            for (Object obj : exportedObjects.values()) {
                Class<?> interfaceClass = message.getInterfaceClass();
                if (interfaceClass.isAssignableFrom(obj.getClass())) {
                    return obj;
                }
            }
            throw new ExportedObjectNotFound("No object implementing " + message.getInterfaceClass().getName() +
                    " found");
        }
    }

    private void exportManagementInterface() {
        export(MANAGEMENT_INTERFACE_DESTINATION, new DolphinManagementInterface() {
            @Override
            public void stop() {
                DolphinServer.this.stop();
            }

            @Override
            public boolean isRunning() {
                return DolphinServer.this.isRunning();
            }
        });
    }

    private Serializable invoke(Object instance, MethodCall call) throws InvocationTargetException, IllegalAccessException {
        Method method = ReflectionUtils.findMatchingMethod(
                instance.getClass(),
                call.getMethodName(), call.getArgs());
        if (method == null) {
            throw new IllegalArgumentException("Unable to find method " + call + " on class " +
                    instance.getClass());
        }
        return (Serializable) method.invoke(instance, call.getArgs());
    }

    /**
     * Stops the server.
     */
    public void stop() {
        if (!isRunning()) {
            throw new IllegalStateException("Server is not running!");
        }
        socketListener.stop();
        socketListener = null;
    }

    /**
     * Check if the server is running.
     *
     * @return true if the server is running, false otherwise.
     */
    public boolean isRunning() {
        return socketListener != null;
    }

}