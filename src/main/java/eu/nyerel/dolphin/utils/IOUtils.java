package eu.nyerel.dolphin.utils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class IOUtils {

    public static void closeQuietly(Closeable output) {
        try {
            if (output != null) {
                output.close();
            }
        } catch (IOException ioe) {
            // ignore
        }
    }

    public static int copy(InputStream is, OutputStream os) throws IOException {
        final byte[] buffer = new byte[10 * 1024];
        int count;
        int total = 0;
        while ((count = is.read(buffer)) > 0) {
            os.write(buffer, 0, count);
            total += count;
        }
        return total;
    }

}
