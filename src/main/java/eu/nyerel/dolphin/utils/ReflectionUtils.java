package eu.nyerel.dolphin.utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class ReflectionUtils {

    private static Map<Class, Class> PRIMITIVE_WRAPPER_MAP = new HashMap<Class, Class>();

    static {
        PRIMITIVE_WRAPPER_MAP.put(Boolean.TYPE, Boolean.class);
        PRIMITIVE_WRAPPER_MAP.put(Byte.TYPE, Byte.class);
        PRIMITIVE_WRAPPER_MAP.put(Character.TYPE, Character.class);
        PRIMITIVE_WRAPPER_MAP.put(Short.TYPE, Short.class);
        PRIMITIVE_WRAPPER_MAP.put(Integer.TYPE, Integer.class);
        PRIMITIVE_WRAPPER_MAP.put(Long.TYPE, Long.class);
        PRIMITIVE_WRAPPER_MAP.put(Double.TYPE, Double.class);
        PRIMITIVE_WRAPPER_MAP.put(Float.TYPE, Float.class);
    }

    public static Method findMatchingMethod(Class<?> clazz, String methodName, Object[] parameters) {
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName) && methodParametersMatch(method, parameters)) {
                return method;
            }
        }
        return null;
    }

    private static boolean methodParametersMatch(Method method, Object[] parameters) {
        Class<?>[] expectedTypes = method.getParameterTypes();
        if (parameters.length == 0 && expectedTypes.length == 0) {
            return true;
        } else {
            for (int i = 0; i < parameters.length; i++) {
                if (isAssignable(expectedTypes[i], parameters[i].getClass())) {
                    return true;
                }
            }
            return false;
        }
    }

    private static boolean isAssignable(Class<?> c1, Class<?> c2) {
        if (c1.isPrimitive()) {
            c1 = PRIMITIVE_WRAPPER_MAP.get(c1);
        }
        if (c2.isPrimitive()) {
            c2 = PRIMITIVE_WRAPPER_MAP.get(c2);
        }
        return c1.isAssignableFrom(c2);
    }


}
