package eu.nyerel.dolphin.core.server;

import eu.nyerel.dolphin.core.client.DolphinObjectFactory;
import eu.nyerel.dolphin.exception.DolphinException;
import eu.nyerel.dolphin.exception.ExportedObjectNotFound;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
public class DolphinServerTest {

    private DolphinServer server;
    private DolphinObjectFactory factory = new DolphinObjectFactory();

    private interface TestInterface {
        int add(int a, int b);
    }

    @Before
    public void init() {
        server = new DolphinServer();
    }

    @After
    public void cleanup() {
        server.stop();
    }

    @Test
    public void testManagementInterfaceExported() {
        server.start();
        DolphinManagementInterface serverManagement = factory.create(DolphinManagementInterface.class);
        assertTrue(serverManagement.isRunning());
    }

    @Test
    public void testManagementInterfaceNotExported() {
        server.setExportManagementInterface(false);
        server.start();
        DolphinManagementInterface serverManagement = factory.create(DolphinManagementInterface.class);
        try {
            serverManagement.isRunning();
            fail("Management interface should not be exported");
        } catch (DolphinException e) {
            assertEquals(ExportedObjectNotFound.class, e.getCause().getClass());
        }
    }

    @Test
    public void testTwoExportedObjectsWithSameInterface() {
        server.export("test1", new TestInterface() {
            public int add(int a, int b) {
                return a + b;
            }
        });
        server.export("test2", new TestInterface() {
            public int add(int a, int b) {
                return a * b;
            }
        });
        server.start();

        TestInterface obj1 = factory.create("test1", TestInterface.class);
        TestInterface obj2 = factory.create("test2", TestInterface.class);
        assertEquals(5, obj1.add(2, 3));
        assertEquals(6, obj2.add(2, 3));

        assertNotNull(factory.create(TestInterface.class));
    }

}