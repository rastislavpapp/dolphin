# Dolphin RMI Library #

Simple Java library, usable as a replacement for Java RMI. Easy to use, with simple and clean API, and no additional dependencies.